```
Usage: pip2tgz [OPTIONS] PACKAGE...
Make Slackware packages from PIP modules.
Options:
    --help                  Show this help message.
    --version               Show version info and exit.
    -2                      Build a Python 2 package.
    -3                      Build a Python 3 package (default).
    --arch ARCH             Specifies the package architecture.
    --build-tag TAG         Specifies the package build tag.
    --build-number NUM      Specifies the package build number.
    --pkgdir DIR            Output packages to DIR (default: /tmp).
    --no-banner             Leave out the pip2tgz banner in the description.
    --pip CMD               Use CMD as the PIP binary (default: pip3).
    --prefix PREFIX         Use "{PREFIX}-PACKAGE" as the generated package
                              prefix (default is python3-).
    --alt-prefixes LIST     Colon-separated list of alternative package
                              prefixes, e.g. python-:python3-: (default:
                              python-).
    --package-ext EXT       Specifies the package extension (default: txz).
                              Ignored if --no-makepkg is specified.
    --package-name,--name NAME Set the exact package name (default:
                              automatically determined by PIP package name).
                              Overrides --prefix.
    --no-makepkg            Do not attempt to run makepkg to build a package.
                              Really only useful with --jail.
    --jail-only JAIL        Set the package jail location to JAIL and don't run
                              makepkg to build a package.
    --jail JAIL             Set the package jail location to JAIL. The script
                              will not clear this directory before outputting
                              its files there. By default, it uses a temporary
                              directory as the build jail.
    --required-hint HINT    Give a hint as to what packages contain which pip
                              packages. Affects the contents of slack-required.
                              Can be specified multiple times. See HINTS.
    --slack-required        Output the slack-required contents and exit.

HINTS:

    Hints take the form of "PIP PACKAGE:PYTHON VER:SLACKWARE PACKAGE BASENAME".
    Examples:
    * 'pycairo:2:python2-module-collection' means 'pycairo' PIP package for
      Python 2.x is contained in the 'python2-module-collection' package
    * 'pyqt5:*:PyQt5' means the 'pyqt5' PIP is contained in the 'PyQt5' package
      for all versions of Python. This would mean that PyQt5 is a package
      containing both Python 2 and Python 3 libraries.

Copyright (C) 2021-2023 Dan Church <h3xx@gmx.com>.
License GPLv3: GNU GPL version 3.0 (https://www.gnu.org/licenses/gpl-3.0.html)
This is free software: you are free to change and redistribute it.
There is NO WARRANTY, to the extent permitted by law.
```

# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

### Added
- Support NO\_COLOR environment variable (see https://no-color.org/)

## Changed
- Print usage error message on stderr
- Update `slack-desc` header format
- Update slack-required hint list (Python package names from -current that do
  not match our naming convention)

### Fixed
- Fix `$JAIL` (now `$OPT_JAIL`) could come from the environment variable
- Omit `/usr/bin/__pycache__` directory
- Account for new `pip3 download` format in PIP v24.0

## [0.2.1] - 2022-12-22

## Changed
- Update `slack-required` hints

### Fixed
- Fixed `--required-hint` help text
- Fixed temp files not being cleaned up

## [0.2.0] - 2022-08-03

### Added
- Add `--required-hint` option for giving hints for generating `slack-required`

## [0.1.4] - 2022-08-03

### Changed
- Exit non-zero if called with no packages

### Fixed
- Fix error in docs
- Fix docdir & package name when requesting a specific package version
- Fix `slack-required` formatting, missing entries

## [0.1.3] - 2021-10-16

### Fixed
- Fix man pages, docs being installed in `/usr/share`

## [0.1.2] - 2021-10-13

### Added
- Add `--package-name` (`--name`) option

## [0.1.1] - 2021-10-11

### Changed
- Omit `installed-files.txt` from packages
- Omit .json symlinks in doc dir

### Fixed
- Add alternate prefixes (`python-`) to requirements generation
- Fix script continuing if `pip install` fails
- Fix issues due to `LANG` not being a UTF-8 locale
- Fix missing `--prefix` option

## [0.1.0] - 2021-10-11

### Added
- Add `--slack-required` option to only output package requirements

### Changed
- Omit more useless docs
- Always install from source instead of .whl binary

### Fixed
- Prevent deps from being downloaded
- Minor doc improvements
- Fix support for installing non-current versions
- Suppress pip warning re: PATH

## [0.0.1] - 2021-08-07
Initial published version

[Unreleased]: https://codeberg.org/h3xx/pip2tgz/compare/v0.2.1...HEAD
[0.2.1]: https://codeberg.org/h3xx/pip2tgz/compare/v0.2.0...v0.2.1
[0.2.0]: https://codeberg.org/h3xx/pip2tgz/compare/v0.1.4...v0.2.0
[0.1.4]: https://codeberg.org/h3xx/pip2tgz/compare/v0.1.3...v0.1.4
[0.1.3]: https://codeberg.org/h3xx/pip2tgz/compare/v0.1.2...v0.1.3
[0.1.2]: https://codeberg.org/h3xx/pip2tgz/compare/v0.1.1...v0.1.2
[0.1.1]: https://codeberg.org/h3xx/pip2tgz/compare/v0.1.0...v0.1.1
[0.1.0]: https://codeberg.org/h3xx/pip2tgz/compare/v0.0.1...v0.1.0
[0.0.1]: https://codeberg.org/h3xx/pip2tgz/releases/tag/v0.0.1
